const {
  recordsetQuery,
  prepareQuery,
  DB_TYPES,
} = require('./utils/db');

const defaults = {
  pageSize: 500,
};

exports.getFunds = async (req) => {
  const q = `
    SELECT [Period]
      ,[ParentObjectId]
      ,[Funds]
    FROM [BillingPortal].[dba].[v_FundsByObject]
  `;

  return await prepareAndProcess(q, req.query, DB_TYPES.BILLING_PORTAL, 'Period');
}

exports.getDebtsBalances = async (req) => {
  const q = `
    SELECT [AccountId]
      ,[Period]
      ,[EndBalance]
    FROM [BillingPortal].[dba].[v_DebtBalance]
  `;

  return await prepareAndProcess(q, req.query, DB_TYPES.BILLING_PORTAL, 'AccountId');
}

exports.getDebts = async (req) => {
  const q = `
    SELECT [AccountId]
      ,[DebtAmount]
      ,[TotalDebtAmount]
      ,[PaymentTerm]
      ,[Days]
    FROM [BillingPortal].[dba].[v_AccountDebtor]
  `;

  return await prepareAndProcess(q, req.query, DB_TYPES.BILLING_PORTAL, 'AccountId');
}

exports.getInvoices = async (req) => {
  const q = `
  SELECT [BillDocumentId]
    ,[AccountId]
    ,[PayerAccountId] [Customer]
    ,[Date] 
    ,[DocumentNumber] 
    ,[InvoiceNumber] 
    ,[Description]
    ,[Amount]
    ,[VATAmount]
    ,[TotalAmount]
    ,[TotalAmountEur]
    ,[PaymentTerm]
    ,[BillDeliveryTypeCode]
    ,[StatusCode]
    ,[IsDebtDocument]
    ,[OwningSystemUserId]
    ,[OwningBusinessUnitId]
    ,[OwningOrganizationId]
    ,[ModifiedOn]
    ,[ModifiedBy]
    ,[CreatedOn]
    ,[CreatedBy]
    ,[VersionNumber]
    ,[ObjectId]
    ,[Period]
    ,[LateFee]
    ,[DocumentDate]
    ,[DocumentType]
    ,[FileName]
    ,[AccountCode]
  FROM [BillingPortal].[dba].[v_BillDocument]
  ${req.query.createdOn ? 'WHERE [CreatedOn] > \'' + req.query.createdOn + '\'' : ''}
  `;
  console.log(q);
  return await prepareAndProcess(q, req.query, DB_TYPES.BILLING_PORTAL, 'BillDocumentId');
}

exports.getUnits = async (req) => {
  const q = `
    SELECT
    b.object_id
    ,c.Quantity [Area Total]
    ,b.house_id 
    ,b.customer_id
    ,b.business_unit [business unit]
    ,b.object_code [legacy number]
    ,b.street
    ,b.BuildingNumber [Building Number]
    ,b.ApartmentNumber [Apartment Number]
    ,b.EntityName [Entity Name]
    ,b.IsCommunalObject [Is Communal Object]
    ,b.Description
    ,b.Description1
    ,b.FullAddress [Full Address]
    /*,b.IsServiceEnabled*/
    ,b.IsDeleted
  FROM
  (
  SELECT  [ObjectId] as [object_id]
      ,[ParentObjectId] as [house_id]
      ,[AccountId] as [customer_id]
      ,[Billing].[dba].crm_BusinessUnit.Name [business_unit]

      ,[Billing].[dba].[Object].[Code] as [object_code]
      ,[Billing].[dba].street.Name [street]
      ,[Billing].[dba].Address.BuildingNumber 
      ,[Billing].[dba].Address.ApartmentNumber
      ,[Billing].[dba].Address.EntityName
      ,[PropertyTypeCode]
      ,[Billing].[dba].[Object].[ModifiedOn]
      ,[Billing].[dba].[Object].[ModifiedBy]
      ,[Billing].[dba].[Object].[CreatedOn]
      ,[Billing].[dba].[Object].[CreatedBy]
      ,[Billing].[dba].[Object].[IsCommunalObject]
      ,[Billing].[dba].[Object].[Description]
      ,[Billing].[dba].[Object].[FullAddress]
      ,[IsServiceEnabled]
      ,[Billing].[dba].[Object].[IsDeleted]
      ,[EntranceId]
      ,[ObjectGroupId]
      ,[Billing].[dba].[Object].[ObjectPurposeId]
      ,[Description1]
      ,[user_IsFundsPortal]
    
    FROM [Billing].[dba].[Object]
    left join [Billing].[dba].Address
    on [Billing].[dba].Address.AddressId=[Billing].[dba].[Object].AddressId
    left join [Billing].[dba].Street
    on [Billing].[dba].Street.StreetId=[Billing].[dba].Address.StreetId
    left join [Billing].[dba].crm_BusinessUnit
    on [Billing].[dba].crm_BusinessUnit.[BusinessUnitId]= [Billing].[dba].[Object].OwningBusinessUnitId
    left join [Billing].[dba].ObjectPurpose
    on  [Billing].[dba].[Object].ObjectPurposeId=[Billing].[dba].ObjectPurpose.ObjectPurposeId 

    
      where ParentObjectId is not null and [Billing].[dba].[Object].IsDeleted=0
    )b
    left join 
    (
    SELECT * FROM
  (
  SELECT [ObjectId]
  ,[ModifiedOn]
  ,[Quantity]
  ,row_number() over(partition by [objectid] order by quantity desc) last_quantity

    FROM [Billing].[dba].[ObjectIndicator]
    )a
    where last_quantity=1
    )c 
    On c.ObjectId=b.object_id
    ${req.query.createdOn ? 'where  ISNULL(b.[ModifiedOn],  b.[CreatedOn]) > \'' + req.query.createdOn + '\'' : ''}
  `;

  return await prepareAndProcess(q, req.query, DB_TYPES.BILLING, 'b.object_id');
}

exports.getMeters = async (req) => {
  const q = `
    SELECT [MeterId]
      ,[ObjectId_obsolete]
      ,[ParentMeterId_obsolete]
      ,[Code]
      ,[NumberCount]
      ,[TypeCode]
      ,[OwningSystemUserId]
      ,[OwningBusinessUnitId]
      ,[OwningOrganizationId]
      ,[ModifiedOn]
      ,[ModifiedBy]
      ,[CreatedOn]
      ,[CreatedBy]
      ,[VersionNumber]
      ,[AssetCode]
      ,[SettingDate]
      ,[RevisionDate]
      ,[Quantity]
      ,[ExpiryDate]
      ,[Diameter]
      ,[Description]
      ,[MetrologicalInspection]
      ,[IsOwnedByAccount]
      ,[IsRemoteAccessEnabled]
      ,[IsDeleted]
      ,[StatusCode]
      ,[MeterModelId]
      ,[SerialNumber]
      ,[BillOperationTemplateId]
      ,[ExternalImport]
    FROM [Billing].[dba].[Meter]
    ${req.query.createdOn ? 'Where ISNULL([Billing].[dba].[Meter].[ModifiedOn],  [Billing].[dba].[Meter].[CreatedOn]) > \'' + req.query.createdOn + '\'' : ''}
  `;

  return await prepareAndProcess(q, req.query, DB_TYPES.BILLING, 'MeterId');
}

exports.getAccounts = async (req) => {
  const q = `
    SELECT [AccountId]
      ,[AddressId]
      ,[BankId]
      ,[Name]
      ,[AccountTypeCode]
      ,[Code]
      ,[CoCode]
      ,[VATCode]
      ,[CreditLimit]
      ,[CreditOnHold]
      ,[Description]
      ,[EMailAddress]
      ,[EMailAddress2]
      ,[Fax]
      ,[PaymentDelay]
      ,[Telephone]
      ,[TelephoneCel]
      ,[Telephone2]
      ,[WebSiteURL]
      ,[DirectDebit]
      ,[DebtorStatus]
      ,[BankAccountNr]
      ,[BillDeliveryTypeCode]
      ,[OwningSystemUserId]
      ,[OwningBusinessUnitId]
      ,[OwningOrganizationId]
      ,[ModifiedOn]
      ,[ModifiedBy]
      ,[CreatedOn]
      ,[CreatedBy]
      ,[VersionNumber]
      ,[FirstName]
      ,[LastName]
      ,[DebtorDescription]
      ,[CustomBit01]
      ,[LateFee]
      ,[BuhAccountId]
      ,[GenerateInvoice]
      ,[CompanyTypeId]
      ,[IsSubContractor]
      ,[IsSupplier]
      ,[ContactId]
      ,[Status]
      ,[Category]
      ,[Priority]
      ,[ReactionTime]
      ,[Scale]
      ,[RelatedCompany]
      ,[Rating]
      ,[DirectDebitFrom]
      ,[DirectDebitTo]
      ,[DirectDebitAmountLimit]
      ,[DirectDebitContractNr]
      ,[BillByAccount]
      ,[IsReminderImmune]
      ,[AccountPassword]
      ,[BillDeliveryPortal]
      ,[InterestFee]
      ,[InterestTerm]
      ,[IsLateFeeDisabled]
      ,[IsDeleted]
      ,[ParentAccountId]
      ,[ReversedVAT]
      ,[AccountGroupId]
      ,[ContractDate]
      ,[ContractRenewDate]
      ,[ESOCode]
      ,[DateOfBirth]
      ,[DateOfDeath]
      ,[IsDead]
      ,[LegacyCode]
    FROM [Billing].dba.[Account]
    ${req.query.createdOn ? 'Where  ISNULL([Billing].dba.[Account].[ModifiedOn],  [Billing].dba.[Account].[CreatedOn]) > \'' + req.query.createdOn + '\'' : ''}
  `;

  return await prepareAndProcess(q, req.query, DB_TYPES.BILLING, 'AccountId');
}

exports.getMeterReadings = async (req) => {
  const q = `
    SELECT [BillOperationId]
      ,[ServicePatternId]
      ,[MeterId]
      ,[ObjectId]
      ,[IndicatorId]
      ,[QuotaId]
      ,[OriginBillOperationId]
      ,[Period]
      ,[OperationTypeCode]
      ,[MeterFrom]
      ,[MeterTo]
      ,[Quantity]
      ,[SubQuantity]
      ,[Amount]
      ,[VATAmount]
      ,[TotalAmount]
      ,[LifeTimeTypeCode]
      ,[RepeatTill]
      ,[RepeatMonthFrom]
      ,[RepeatMonthTo]
      ,[Description]
      ,[Ratio]
      ,[OrderNumber]
      ,[OwningSystemUserId]
      ,[OwningBusinessUnitId]
      ,[OwningOrganizationId]
      ,[ModifiedOn]
      ,[ModifiedBy]
      ,[CreatedOn]
      ,[CreatedBy]
      ,[VersionNumber]
      ,[TariffId]
      ,[IsModified]
      ,[VATPercentage]
      ,[LeasingAmount]
      ,[BillLineText]
      ,[CalculationId]
      ,[SubContractor]
      ,[IsGroupPrimaryRow]
      ,[GroupNumber]
      ,[From]
      ,[Until]
      ,[ImportId]
    FROM [Billing].[dba].[BillOperation]
    ${req.query.createdOn ? 'WHERE ISNULL([Billing].[dba].[BillOperation].[ModifiedOn],  [Billing].[dba].[BillOperation].[CreatedOn]) > \'' + req.query.createdOn + '\'' : ''}
  `;

  return await prepareAndProcess(q, req.query, DB_TYPES.BILLING, 'BillOperationId');
}

exports.getBuildings = async (req) => {
  const q = `
    SELECT [Billing].[dba].crm_BusinessUnit.Name [business_unit]
      ,[Billing].[dba].[Object].[ObjectId]  [house_id]
      ,[Billing].[dba].[Object].[Code] [name]
      ,[Billing].[dba].street.Name [street]
      ,[Billing].[dba].Address.BuildingNumber 
      ,[Billing].[dba].Address.EntityName
      ,[Billing].[dba].[Object].[ModifiedOn]
      ,[Billing].[dba].[Object].[CreatedOn]
      ,[IsCommunalObject]
      ,[Billing].[dba].[Object].[Description] [Legacy Number]
      ,[BillDescription]
      ,[Workspace] 
      ,[Size]   
      ,[BuildYear]
      ,[FloorNumber]
      ,[LandingNumber]
      ,[CellarSpace]
      ,[CleanSpace]
      ,[Estimate]
      ,[Status]
      ,[Billing].[dba].[Object].[FullAddress]
      ,[Billing].[dba].[Object].[IsDeleted]
    ,[Billing].[dba].ObjectPurpose.Description Objekto_paskirtis 
    ,[Description1]  [comment]
    FROM [Billing].[dba].[Object]
      left join [Billing].[dba].Address
      on [Billing].[dba].Address.AddressId=[Billing].[dba].[Object].AddressId
      left join [Billing].[dba].Street
      on [Billing].[dba].Street.StreetId=[Billing].[dba].Address.StreetId
        left join [Billing].[dba].crm_BusinessUnit
        on [Billing].[dba].crm_BusinessUnit.[BusinessUnitId]= [Billing].[dba].[Object].OwningBusinessUnitId
        left join [Billing].[dba].ObjectPurpose
      on  [Billing].[dba].[Object].ObjectPurposeId=[Billing].[dba].ObjectPurpose.ObjectPurposeId 
        where ParentObjectId is null and [Billing].[dba].[Object].IsDeleted=0
    
    ${req.query.createdOn ? 'and ISNULL([Billing].[dba].[Object].[ModifiedOn],  [Billing].[dba].[Object].[CreatedOn]) > \'' + req.query.createdOn + '\'' : ''}
  `;

  return await prepareAndProcess(q, req.query, DB_TYPES.BILLING, 'house_id');
};

// Helpers
const fromUrlQuery = (query) => {
  const {
    page = 0,
    pageSize = defaults.pageSize,
  } = query;

  return { page, pageSize };
}

const prepareAndProcess = async (q, query, dbType, orderBy) => {
  const { page, pageSize } = fromUrlQuery(query);

  q = prepareQuery(q, { orderBy, page, pageSize });

  return await recordsetQuery(q, dbType);
};