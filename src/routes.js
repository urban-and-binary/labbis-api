const { Router } = require('express');
const aw = require('express-async-wrap');

const { requestProcessor } = require('./utils/requests');
const services = require('./services');

const router = new Router();

const routeServiceMap = [
  { url: '/meter-readings', handler: services.getMeterReadings },
  { url: '/buildings', handler: services.getBuildings },
  { url: '/accounts', handler: services.getAccounts },
  { url: '/meters', handler: services.getMeters },
  { url: '/units', handler: services.getUnits },
  { url: '/invoices', handler: services.getInvoices },
  { url: '/debts', handler: services.getDebts },
  { url: '/debts-balances', handler: services.getDebtsBalances },
  { url: '/funds', handler: services.getFunds },
];

routeServiceMap.forEach(({ url, handler }) => {
  router.get(url, aw(async (req, res) => {
    await requestProcessor(req, res, handler);
  }));
});

// Sample for custom-handled route
// router.get('/url-to-endpoint', aw(async (req, res) => {
//   await requestProcessor(req, res, service.someRouteHandlerFn);
// }));

module.exports = router;