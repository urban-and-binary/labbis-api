const dotenv = require('dotenv');
dotenv.config();

const config = {
  apiKeyPrefix: 'Api-Key',
  port: process.env.PORT || 8080,
  dbConnBilling: process.env.DB_CONN_BILLING,
  dbConnBillingPortal: process.env.DB_CONN_BILLING_PORTAL,
  debugSql: process.env.DEBUG_SQL === 'true',
  apiKey: process.env.API_KEY,
  responseTimeoutMs: process.env.RESPONSE_TIMEOUT_MS || 60000,
};

module.exports = config;