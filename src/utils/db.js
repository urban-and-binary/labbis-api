const sql = require('mssql');
const {
  dbConnBilling,
  dbConnBillingPortal,
  debugSql,
} = require('./config');

const DB_TYPES = {
  BILLING: 'billing',
  BILLING_PORTAL: 'billingPortal',
};

const client = async (dbConn) => {
  let conn;
  if (dbConn === DB_TYPES.BILLING) {
    conn = dbConnBilling;
  } else if (dbConn === DB_TYPES.BILLING_PORTAL) {
    conn = dbConnBillingPortal;
  }

  if (!conn) {
    throw new Error(`DB connection unknown: ${dbConn}`);
  }

  try {
    return await new sql.ConnectionPool(conn).connect();
  } catch (err) {
    console.error('Cannot connect to SQL instance', err);
    throw err;
  }
};

const query = async (q, dbConn) => {
  if (!dbConn) {
    throw new Error('Missing DB connection parameter');
  }

  const db = await client(dbConn);

  try {
    const result = await db.request().query(q);

    return result;
  } catch (err) {
    console.error('Error executing SQL query', err);
    // Close DB connection
    sql.close();
    
    throw err;
  }
};

const recordsetQuery = async (q, dbConn) => {
  if (debugSql) {
    console.info(`Q: ${q}`);
  }

  const { recordset } = await query(q, dbConn);

  if (!recordset) {
    throw new Error('Query error');
  }

  return recordset;
};

const prepareQuery = (q, params = {}) => {
  const {
    orderBy,
    page = 0,
    pageSize = 500,
  } = params;

  if (!orderBy) {
    if (debugSql) console.info('Not paginating query result');

    return q;
  }

  const offset = page * pageSize;
  const suffix = `
    ORDER BY ${orderBy} OFFSET ${offset} ROWS FETCH NEXT ${pageSize} ROWS ONLY;
  `;

  return `${q} ${suffix}`;
};

module.exports = { query, recordsetQuery, prepareQuery, DB_TYPES };