const { apiKeyPrefix, apiKey } = require('./config');

const requestProcessor = async (req, res, action) => {
  try {
    const data = await action(req);

    return res.send(data);
  } catch (err) {
    console.error(err.stack);
    
    return res.status(500).send(err.message);
  }
};

const apiKeyValidator = (req, res, next) => {
  if (req.headers.authorization !== `${apiKeyPrefix} ${apiKey}`) {
    res.status(401).send('Unauthorized');
  } else {
    next();
  }
};

module.exports = { requestProcessor, apiKeyValidator };