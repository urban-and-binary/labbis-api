const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const timeout = require('connect-timeout');

const config = require('./src/utils/config');
const { apiKeyValidator } = require('./src/utils/requests');
const routes = require('./src/routes');

const app = express();

app.use(timeout(config.responseTimeoutMs));
app.use(apiKeyValidator);
app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use('/api', routes);

app.listen(config.port, () => {
    console.log(`Server running on port: ${config.port}`)
});