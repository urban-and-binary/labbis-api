## Request requirements
All API calls are exported as Postman collection to `./postman/Labbis API.postman_collection.json`. Also included the JSON for Postman environment config.

Collection env variables:
- base_url - base URL of service i.e. http://127.0.0.1:8080/api
- page_size - response records limit
- page - page number for paginated results
- token - API key for accessing the endpoints

Every request must contain an authorization header with prefix + API key:  
`Authorization: Api-Key <API_KEY from .env file>`

## Endpoints
Base URL: `http://<Server IP>:<Service port>/api`

Exposed endpoints:
- GET /meter-readings
- GET /buildings
- GET /accounts
- GET /meters
- GET /units
- GET /invoices
- GET /debts
- GET /debts-balances
- GET /funds

## Pagination
All GET endpoints return paginated results. Default page size is 500. 

Page size and page number can be controlled by setting URL parameters `pageSize` and `page` i.e. request `https://127.0.0.1:8080/api/pageSize=100&page=5` will skip 500 records and return the next 100.

URL parameters:
- pageSize - record amount limit in response. Defaults to 500. Optional.
- page - page number, offsets the records to skip. Defaults to 0. Optional.

## Environment variables
Service configuration is in `.env` file. An example environment file `.env.example` contains required variable and their example values.

Environment variables:
- PORT - service listening port. Defaults to 8080. Optional.
- DB_CONN_BILLING - Billing DB connection string.
- DB_CONN_BILLING_PORTAL - BillingPortal DB connection string.
- DEBUG_SQL - outputs SQL queries to logs if `true`. Defaults to `false`. Optional.
- API_KEY - secure string for authorizing with the service.

## Deployment notes
NodeJS service is deployed via PM2. PM2 is installed as automatic Windows service.

### Deploy pm2 as a Windows service
Deployment guide: https://thomasswilliams.github.io/development/2020/04/07/installing-pm2-windows.html

### Start API service
Navigate to source code location (i.e. `cd c:\api\labbis-api`) and start the PM2 service with `pm2 start server.js`

## Troubleshooting

### pm2-service-install issues
If `pm2-service-install -n PM2` hangs or fails:
1. Go to `%USERPROFILE%\AppData\Roaming\npm\node_modules\pm2-windows-service`
2. Edit `package.json`
4. Change `inquirer` version to `^7.0.4`
5. Run `npm install`
6. Repeat `pm2-service-install` command

### Code changes not reflected
If anything changes in the code base - service restart is required:
1. Remove the running service: `pm2 delete server`
2. Start new service: `pm2 start server.js`

### .env variables ignored
Make sure that `pm2 start server.js` happens from the same folder that .env file is in.